import numpy as np
import itertools
from sklearn.base import TransformerMixin


class Raiser(TransformerMixin):
    def fit(self, X, y=None):
        return self

    def transform(self, X):
        return X.reshape(-1, 1)


class StringSplitter(TransformerMixin):
    def fit(self, X, y=None):
        return self

    def transform(self, X):
        X = X.astype(str)

        return np.char.split(X, ',')


class IgnoreNotSeen(TransformerMixin):
    def __init__(self, flatten=False):
        self._flatten = flatten

    def fit(self, X, y=None):
        self.classes_ = set(
            itertools.chain.from_iterable(X)
            if self._flatten
            else X
        )

        return self

    def _remove_unseen(self, instance):
        if self._flatten:
            return [value for value in instance if value in self.classes_]
        else:
            return instance if instance in self.classes_ else None

    def transform(self, X):
        remover = np.vectorize(self._remove_unseen, otypes=[object])

        return remover(X)


class ValueReplacer(TransformerMixin):
    def __init__(self, to_replace, replacement):
        self._to_replace = to_replace
        self._replacement = replacement

    def fit(self, X, y=None):
        return self

    def _replace(self, x):
        return self._replacement if x == self._to_replace else x

    def transform(self, X):
        replacer = np.vectorize(self._replace, otypes=[object])

        return replacer(X)


class RelatedMapper(TransformerMixin):
    def __init__(self, relation_mapping, missing_value=np.nan):
        self._relation_mapping = relation_mapping
        self._missing_value = missing_value

    def fit(self, X, y=None):
        return self

    def _mapper(self, instance):
        related = []

        for series_id in instance:
            if series_id in self._relation_mapping:
                related.append(self._relation_mapping[series_id])

        if len(related) > 0:
            return np.mean(related)
        else:
            return self._missing_value

    def transform(self, X):
        X = np.char.split(X.astype(str), ',')
        mapper = np.vectorize(self._mapper)
        return mapper(X)


class ListNGramer(TransformerMixin):
    def __init__(self, ngrams=2):
        self._ngrams = ngrams

    def fit(self, X, y=None):
        return self

    def _ngramer(self, instance):
        result = instance

        if len(instance) >= 2:
            result = sorted(list(instance))

            for index, first_word in enumerate(instance[:-1]):
                for second_word in instance[index + 1:]:
                    result.append('{}-{}'.format(first_word, second_word))

        return result

    def transform(self, X):
        ngramer = np.vectorize(self._ngramer, otypes=[object])
        return ngramer(X)
