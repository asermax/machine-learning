# coding: utf-8
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import util, classifiers
df = pd.read_csv('https://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data')
X = df.iloc[0:100, [0,2]].values
y = df.iloc[0:100, 4].values
y = np.where(y == 'Iris-setosa', -1, 1)
X_std = util.standarize_dataset(X)
