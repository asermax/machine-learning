from . import base
import numpy as np

__all__ = ['Adaline']


class BaseGradientDescent(object):
    def __init__(self, adaline, *args, **kwargs):
        self.ada = adaline


class BatchGradientDescent(BaseGradientDescent):
    def apply(self, X, y):
        self.ada.w_ = np.zeros(X.shape[1] + 1)
        self.ada.cost_ = []

        for i in range(self.ada.n_iter):
            output = self.ada.net_input(X)
            errors = (y - output)

            self.ada.w_[1:] += self.ada.eta * X.T.dot(errors)
            self.ada.w_[0] *= self.ada.eta * errors.sum()

            cost = (errors**2).sum() / 2.0
            self.ada.cost_.append(cost)

    def partial_apply(self, X, y):
        raise NotImplementedError


class StochasticGradientDescent(BaseGradientDescent):
    def __init__(self, adaline, shuffle):
        super().__init__(adaline)
        self.shuffle = shuffle

    def apply(self, X, y):
        self._initialize_weights(X)
        self.ada.cost_ = []

        for i in range(self.ada.n_iter):
            if self.shuffle:
                X, y = self._shuffle(X, y)

            cost = []

            for xi, target in zip(X, y):
                cost.append(self._update_weights(xi, target))

            avg_cost = sum(cost) / len(y)
            self.ada.cost_.append(avg_cost)

    def partial_apply(self, X, y):
        if not self.w_initialized:
            self._initialize_weights(X)

        if y.ravel().shape[0] > 1:
            for xi, target in zip(X, y):
                self._update_weights(xi, target)
        else:
            self._update_weights(X, y)

    def _initialize_weights(self, X):
        dimension = X.shape[1]
        self.ada.w_ = np.zeros(1 + dimension)
        self.w_initialized = True

    def _shuffle(self, X, y):
        r = np.random.permutation(len(y))

        return X[r], y[r]

    def _update_weights(self, xi, target):
        output = self.ada.net_input(xi)
        error = target - output
        self.ada.w_[1:] += self.ada.eta * xi.dot(error)
        self.ada.w_[0] += self.ada.eta * error
        cost = 0.5 * error**2

        return cost


class Adaline(base.Classifier):
    BATCH = BatchGradientDescent
    STOCHASTIC = StochasticGradientDescent

    def __init__(
        self, eta=0.01, n_iter=50, gradient_strategy=STOCHASTIC, shuffle=True
    ):
        super().__init__(eta=eta, n_iter=n_iter)
        self.gradient_strategy = gradient_strategy(self, shuffle)

    def fit(self, X, y):
        self.gradient_strategy.apply(X, y)

        return self

    def partial_fit(self, X, y):
        self.gradient_strategy.apply(X, y)

        return self
