from . import base
import numpy as np

__all__ = ['Perceptron']


class Perceptron(base.Classifier):
    def fit(self, X, y):
        self.w_ = np.zeros(X.shape[1] + 1)
        self.errors_ = []

        for _ in range(self.n_iter):
            errors = 0

            for xi, target in zip(X, y):
                update = self.eta * (target - self.predict(xi))
                self.w_[1:] += update * xi
                self.w_[0] += update
                errors += int(update != 0.0)

            self.errors_.append(errors)

        return self
