# coding: utf-8
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import util, perceptron
import util, perceptron
get_ipython().magic('load_ext autoreload')
get_ipython().magic('autoreload 1')
get_ipython().magic('aimport util')
get_ipython().magic('aimport perceptron')
df = pd.read_csv('https://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data')
X = df.iloc[0:100, [0,2]].values
y = df.iloc[0:100, 4].values
y = np.where(y == 'Iris-setosa', -1, 1)
ppn = perceptron.Perceptron(eta=0.1, n_iter=10)
ppn.fit(X, y)
util.plot_decision_regions(X, y, classifier=ppn)
plt.xlabel('sepal length [cm]')
plt.xlabel('petal length [cm]')
plt.legend(loc='upper left')
plt.show()
